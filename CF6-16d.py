import math

PI = math.pi
sqrt = math.sqrt
sin = math.sin


nreal = 4
nbin = 0
ncon = 2
nobj = 2

min_realvar = [0.0,-2.0,-2.0,-2.0,-2.0,-2.0,-2.0,-2.0,-2.0,-2.0,-2.0,-2.0,-2.0,-2.0,-2.0,-2.0]
max_realvar = [1.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0]

def MYSIGN(x):
    if(x>0):
        return 1.0
    else:
        return -1.0
 
def test_problem(xreal,xbin,gene,obj,constr):
    sum1=0
    sum2=0
    yj=0
    for j in range(2,nreal+1):
        if(j%2==1):
            yj = xreal[j-1] - 0.8*xreal[0]*math.cos(6.0*math.pi*xreal[0] + j*math.pi/nreal)
            sum1 += yj*yj
        else:
            yj = xreal[j-1] - 0.8*xreal[0]*math.sin(6.0*math.pi*xreal[0] + j*math.pi/nreal)
            sum2 += yj*yj
    obj[0] = xreal[0] + sum1
    obj[1] = (1.0 - xreal[0])*(1.0 - xreal[0]) + sum2
    constr[0] = xreal[1]-0.8*xreal[0]*sin(6.0*xreal[0]*PI+2.0*PI/nreal) - MYSIGN((xreal[0]-0.5)*(1.0-xreal[0]))*sqrt(abs((xreal[0]-0.5)*(1.0-xreal[0])))
    constr[1] = xreal[3]-0.8*xreal[0]*sin(6.0*xreal[0]*PI+4.0*PI/nreal) - MYSIGN(0.25*sqrt(1-xreal[0])-0.5*(1.0-xreal[0]))*sqrt(abs(0.25*sqrt(1-xreal[0])-0.5*(1.0-xreal[0])))