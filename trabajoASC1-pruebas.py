from curses import color_pair
from inspect import _void
import math
from math import sqrt
import random
import matplotlib.pyplot as plt



N = 40 #Número de subproblemas
G = 100 # Número de generaciones
T = 0.3 #tamaño vecindad, entre un 10% y un 30% del número de subproblemas
preference = [] #z
poblacion = []
evaluacion_global = []

#Para pruebas
contx = 0
conty = 0


#Calculo de las distancia euclidea entre dos puntos
def distancia_euclidea(p1,p2):
    return(sqrt((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2))

#Inicializar vectores peso equiespaciados forma euclidea
def inicializar_vectores_alpha():
    vector = list()
    for j in range(0,N):
        vector.append([0.0,0.0])

    peso = 1/(N-1)
    vector[0][0] = 0.0
    vector[0][1] = 1.0
    suma = list()
    for i in range(1, N-1):
        vector[i][0] = vector[i-1][0] + peso
        vector[i][1] = vector[i-1][1] - peso 
        suma.append(vector[i][0] + vector[i][1])
    
    vector[N-1][0] = 1.0
    vector[N-1][1] = 0.0
    return vector

#conjunto B(i)
def distancia_vectores(vectores_alpha):
    listaVecinos = list()
    for i in range(0,N):
        lista = list()
        for j in range(0,N):
            d = distancia_euclidea(vectores_alpha[i],vectores_alpha[j])
            lista.append((j,d))
        lista.sort(key=lambda x:x[1])      
        listaVecinos.append(lista)

    numeroVecinos = int(len(vectores_alpha)*T)

    for k in range(0,N):
        l = listaVecinos[k]
        lista_comprimida = list()
        for z in range(0,numeroVecinos):
            lista_comprimida.append(l[z])
        listaVecinos[k] = lista_comprimida
    
    return listaVecinos
    
    
def inicializar_poblacion():
    global poblacion
    poblacion = list()
    for i in range(0,N):
        individuo = list()
        for j in range(0,30):
            individuo.append(random.uniform(0,1))
        poblacion.append(individuo)
    return poblacion

def funcion_zdt3(individuo):
    PI = math.pi
    tmp=0.0
    f1 = individuo[0]

    for i in range(1,len(individuo)):
        tmp+=individuo[i]
    
    g = 1+((9*tmp)/(len(individuo)-1))
    h = 1-sqrt(f1/g)-(f1/g)*math.sin(10*PI*f1)
    f2 = g*h

    evaluacion_local = [f1,f2]

    return evaluacion_local


def ev_global(poblacion):
    global evaluacion_global
    
    for individuo in poblacion:
        evaluacionLocal = funcion_zdt3(individuo)
        evaluacion_global.append(evaluacionLocal)
    

def inicializar_z(evaluacion_global):
    global preference
    cx = 0
    cy = 0
    preference.append(evaluacion_global[0][0])
    preference.append(evaluacion_global[0][1])

    for i in range(1, N):
        if(evaluacion_global[i][0] < preference[0]):
            preference[0] = evaluacion_global[i][0]
            cx+=1 #ver cuantas veces cambia el punto de referencia[0] para pruebas
        if(evaluacion_global[i][1] < preference[1]):
            preference[1] = evaluacion_global[i][1]
            cy+=1 #ver cuantas veces cambia el punto de referencia[1] para pruebas
    return(cx,cy)

def reproduccion_individuo(poblacion,conjuntoB,indice):
    
    vecinos = conjuntoB[indice]
    random.shuffle(vecinos)
    #mutacion = [e1 + 0.5*(e2-e3) for e1,e2,e3 in zip(poblacion[vecinos[0][0]],poblacion[vecinos[1][0]],poblacion[vecinos[2][0]])]
    mutacion = [] 
    for j in range(0,30):
        e1 = poblacion[vecinos[0][0]][j]
        e2 = poblacion[vecinos[1][0]][j]
        e3 = poblacion[vecinos[2][0]][j]
        mutacion.append(e1 + 0.5*(e2-e3))
    mutacion_final = list()
    for elemento in mutacion: #comprobamos que no sea mayor a 1 ni menor a 0
        if(elemento < 0):
            mutacion_final.append(0)
        elif(elemento>1):
            mutacion_final.append(1)  #1-(elemento-1))
        else:
            mutacion_final.append(elemento)
    vector_cruce = list()
    nr = random.randint(0,29)
    for i in range(0,30): #cruce
        if (random.uniform(0,1) <= 0.5 or nr == i): #añadimos genotipo mutante
            vector_cruce.append(mutacion_final[i])
        else: #añadimos genotipo vector de la iteracion
            vector_cruce.append(poblacion[indice][i])
    
    return vector_cruce

def Tchebychef(peso,evaluacion):
    global preference
    cX = peso[0] * abs((evaluacion[0]-preference[0]))
    cY = peso[1] * abs((evaluacion[1]-preference[1]))
    return max(cX,cY)
        

                
def bucle(poblacion,conjuntoB,vectores_alpha):
    global contx,conty,preference
    for i in range(0,N):
        vector_cruce = reproduccion_individuo(poblacion,conjuntoB,i)
        evaluacion_individuo = funcion_zdt3(vector_cruce) #evaluamos el vector cruce
        if(preference[0]>evaluacion_individuo[0]):
            preference[0] = evaluacion_individuo[0]
            #contx+=1 #contador para ver cuantas veces se actualiza la z[0](pruebas)
        if(preference[1]>evaluacion_individuo[1]):
            preference[1] = evaluacion_individuo[1]
            #conty+=1 #contador para ver cuantas veces se actualiza la z[1](pruebas)
        
        for vecino in conjuntoB[i]:
            if(Tchebychef(vectores_alpha[vecino[0]],funcion_zdt3(poblacion[vecino[0]])) >= Tchebychef(vectores_alpha[vecino[0]],evaluacion_individuo)):
                poblacion[vecino[0]] = vector_cruce
                evaluacion_global[vecino[0]] = evaluacion_individuo
    
        

if __name__ == "__main__":

    poblacion = inicializar_poblacion()
    vectores_alpha = inicializar_vectores_alpha()
    # suma = []
    # for vector in vectores_alpha:
    #     suma.append(vector[0] + vector[1])
    #print("vectores alpha\n")
    #print(vectores_alpha)
    conjuntoB = distancia_vectores(vectores_alpha)
    #print("subproblema: vecinos")
    #print(conjuntoB)
    c = inicializar_z(poblacion)  # Esto tiene que estar aqui porq tiene que haber una z con la poblacion inicial
    eG = ev_global(poblacion)
    
    #print("Evaluaciones poblacion inicial")
    #print(eG)
    #print("\n")
    #iteraciones
    for i in range(0,G):
        bucle(poblacion,conjuntoB,vectores_alpha)
        eG1 = ev_global(poblacion)
        #print("Evaluacion después de recorrer cada subproblema")
        #print(eG1)
    #print(contx,conty)
    #eGN = ev_global(poblacion)
    #print(eGN)
    x = []
    y = [] 
    for i in range(0,N):
        # x.append((eG[i][0],eG1[i][0]))
        # y.append((eG[i][1],eG1[i][1])) DALE ves la ejecucion?
        x.append(evaluacion_global[i][0])
        y.append(evaluacion_global[i][1])

    with open('evaluacionesPrueba.txt', 'w') as f:
        #f.write("f1 Inicial,    f1 tras 1 evolucion,    f2 inicial,     f2 tras 1 evolucion \n")
        for i in range(0,len(x)):
            f.write(str(x[i]) + "    " +str(y[i])+"\n")
        f.close()


    